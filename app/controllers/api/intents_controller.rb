class Api::IntentsController < ActionController::API
  before_action :doorkeeper_authorize!

  def create
    head 201
  end
end
