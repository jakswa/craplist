class ApplicationController < ActionController::Base
  before_action :check_oauth_redirect

  private

  def check_oauth_redirect
    redirect_to session[:redirect_after] if session[:redirect_after]
  end
end
